﻿List<int> numbers = new List<int> { 2, 2, 3, 4, 5 };
int singleNumber3 = numbers.Single(x => x == 3); // 3
int singleNumber0 = numbers.Single(x => x == 0); // throw exception vì không tìm thấy
int singleNumber2 = numbers.Single(x => x == 2); // throw exception vì xuất hiện 2 phần tử

public static class ListExtension
{
    public static T Single<T>(this List<T> numbers, Func<T, bool> predicate)
    {
        T result = default;
        bool found = false;

        foreach (T number in numbers)
        {
            if (predicate(number))
            {
                if (found)
                {
                    throw new InvalidOperationException("More than one element matches the predicate.");
                }
                found = true;
                result = number;
            }
        }

        if (!found)
        {
            throw new InvalidOperationException("No element matches the predicate.");
        }

        return result;
    }
}
